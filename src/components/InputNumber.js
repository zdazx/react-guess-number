import React from 'react';
import '../components_less/InputNumber.less'
class InputNumber extends React.Component{
  constructor(props){
    super(props);

    this.guessNum = this.guessNum.bind(this);
  }

  guessNum(event){
    this.props.inputNum(event.target.value);
  }

  render() {
    return (
      <section className={'InputNumber'}>
        <input type={'text'} className={'inputNum'} maxLength={1} onChange={this.guessNum}/>
        <input type={'text'} className={'inputNum'} maxLength={1} onChange={this.guessNum}/>
        <input type={'text'} className={'inputNum'} maxLength={1} onChange={this.guessNum}/>
        <input type={'text'} className={'inputNum'} maxLength={1} onChange={this.guessNum}/>
      </section>
    );
  }
}

export default InputNumber;