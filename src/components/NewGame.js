import React from 'react';
import '../api/API'
import {get, post} from "../api/API";

class NewGame extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      gameId: 0,
      gameAnswer: '',
      gameUrl: ''
    };
    this.generateGame = this.generateGame.bind(this);
  }

  generateGame(event){
    event.preventDefault();
    post('http://localhost:8080/api/games')
      .then(res => {
        const urlNew = res.headers.get("location");
        this.setState({gameUrl:urlNew});
        const getUrl = 'http://localhost:8080' + urlNew;
        get(getUrl).then(res => res.json()).then(json => {
          const {id, answer} = json;
          this.props.newGame(id, answer);
        })
      });
  }

  render() {
    return (
      <form>
        <button type='submit' onClick={this.generateGame}>New Game</button>
      </form>

    );
  }

}

export default NewGame;