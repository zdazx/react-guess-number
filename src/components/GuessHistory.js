import React, {Component} from 'react';

class GuessHistory extends Component{
  render() {
    return (
      <section>
        <ul>
          {
            this.props.guessHistory.filter(item => {
              return <li>item</li>
            })
          }
        </ul>
      </section>
    );
  }
}

export default GuessHistory;