import React from 'react';
import {patch} from "../api/API";

class Guess extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      gameId: 0
    }
    this.guessGame = this.guessGame.bind(this);
  }

  guessGame(event){
    event.preventDefault();
    const gameId = this.props.gameId;
    this.setState({gameId: gameId}, () => {
      const url = 'http://localhost:8080/api/games/' + this.state.gameId;
      patch(url, this.props.guessNum)
        .then(res => res.json())
        .then(json => {
          const {hint, correct} = json;
          this.props.guessResult(hint);
        })
    })
  }

  render() {
    return (
      <section>
        <form>
          <button type={'submit'} onClick={this.guessGame}>Guess</button>
        </form>
      </section>
    );
  }
}

export default Guess;