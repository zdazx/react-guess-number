import React, {Component} from 'react';
import './App.less';
import NewGame from "./components/NewGame";
import InputNumber from "./components/InputNumber";
import GuessHistory from "./components/GuessHistory";
import Guess from "./components/Guess";

class App extends Component{

  constructor(props) {
    super(props);
    this.state = {
      id: 0,
      answer: '',
      guessNum: '',
      guessHistory: []
    }
    this.newGame = this.newGame.bind(this);
    this.inputNum = this.inputNum.bind(this);
    this.guessResult = this.guessResult.bind(this);
  }

  newGame (id, answer){
    this.setState({id: id,answer: answer});
  }

  inputNum(input){
    const {guessNum} = this.state;
    const guess = guessNum + input;
    this.setState({guessNum: guess})
  }

  guessResult(result){
    const {guessHistory} = this.state;
    guessHistory.push(result)
    this.setState({guessHistory});
  }

  render() {
    return (
      <div className='App'>
        <NewGame newGame={this.newGame} />
        <InputNumber inputNum={this.inputNum} />
        <GuessHistory guessHistory={this.state.guessHistory} />
        <Guess gameId={this.state.id} guessNum={this.state.guessNum} guessResult={this.guessResult}/>
      </div>
    );
  }
}

export default App;