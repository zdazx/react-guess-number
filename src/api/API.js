export const get = (url) => {
  return fetch(url);
};

export const post = (url, data) => {
  return fetch(url, {
    method: 'POST'
  });
};

export const patch = (url, data) => {
  return fetch(url, {
    method: 'PATCH',
    body: JSON.stringify({'\'answer\'': data})
  })
};